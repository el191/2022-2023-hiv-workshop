
### Introduction to High-throughput Sequencing (3/11/2022)
  - [High-Throughput Sequencing Theory](rnaseq_bioinformatics/1_hts_background.pdf)
  - [Bioinformatics Overview](rnaseq_bioinformatics/3_bioinformatics_overview.pdf)
  - [Quality Score Primer](rnaseq_bioinformatics/notebooks/quality_scores.md)
  
### Bioinformatics for RNA-seq (3/18/2022)
  - [Bulk RNA-Seq Table of Contents](rnaseq_bioinformatics/bulk_rnaseq_TOC.md)

### Statistical Analysis for RNA-seq  (3/25/2022)
  - [Lecture Slides](rnaseq_statistics/PS_DESeq_stat.pdf)
  - [DESeq2: Data Preparation](rnaseq_statistics/deseq_prepare_data.Rmd)
  - [DESeq2: DESeq Analysis and Plotting](rnaseq_statistics/deseq_analysis_plot.Rmd)

### Microbiome Analysis (4/1/2022)
  - [Microbiome Table of Contents](assays/microbiome/microbiome_toc.md)

### Bioinformatics for scRNA-seq (4/8/2022)
  - [scRNA-Seq Table of Contents](sc_rnaseq/sc_rnaseq_toc.md)

### Bioinformatics for Flow Cytometry  (4/15/2022)
  - Flow Cytometry Table of Contents
